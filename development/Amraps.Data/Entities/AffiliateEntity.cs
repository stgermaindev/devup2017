using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amraps.Data.Entities
{
    [Table("Affiliates")]
    public class AffiliateEntity
    {
        public AffiliateEntity()
        {
            Locations = new HashSet<LocationEntity>();
        }

        [Key]
        public int AffiliateId { get; set; }

        [StringLength(64)]
        public string Name { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(256)]
        public string EmailAddress { get; set; }

        [StringLength(1024)]
        public string Website { get; set; }

        [StringLength(256)]
        public string PlaceId { get; set; }

        [Required]
        [StringLength(128)]
        public string StreetAddress { get; set; }

        [StringLength(128)]
        public string StreetAddress2 { get; set; }

        [Required]
        [StringLength(128)]
        public string City { get; set; }

        [Required]
        [StringLength(64)]
        public string State { get; set; }

        [Required]
        [StringLength(24)]
        public string PostalCode { get; set; }

        [Required]
        [StringLength(128)]
        public string CreatedByUser { get; set; }

        [InverseProperty("Affiliate")]
        public ICollection<LocationEntity> Locations { get; set; }
    }
}