﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Amraps.Data.Entities
{
    public class AppUser : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? DefaultLocationId { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("DefaultLocationId")]
        [InverseProperty("LocationDefaultUsers")]
        public LocationEntity DefaultLocation { get; set; }
    }
}
