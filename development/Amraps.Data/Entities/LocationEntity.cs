﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Serilog.Parsing;

namespace Amraps.Data.Entities
{
    [Table("Locations")]
    public class LocationEntity
    {
        public LocationEntity()
        {
            LocationDefaultUsers = new HashSet<AppUser>();
        }

        [Key]
        public int LocationId { get; set; }

        public int AffiliateId { get; set; }

        [StringLength(64)]
        public string Name { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }

        [ForeignKey("AffiliateId")]
        [InverseProperty("Locations")]
        public AffiliateEntity Affiliate { get; set; }

        [InverseProperty("DefaultLocation")]
        public ICollection<AppUser> LocationDefaultUsers { get; set; }

    }
}
