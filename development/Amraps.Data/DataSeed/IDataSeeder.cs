﻿using System;
using System.Collections.Generic;
using System.Linq;
using Amraps.Data.Entities;

namespace Amraps.Data.DataSeed
{
    public interface IDataSeeder
    {
        IDataSeeder SeedData();
    }

    public class DevDataSeeder : IDataSeeder
    {
        private readonly AmrapsDbContext _context;
        public DevDataSeeder(AmrapsDbContext context)
        {
            _context = context;
        }

        public IDataSeeder SeedData()
        {
            if (_context.Users.Any(u => u.UserName.ToLower() == "master_overseer"))
                return this;

            var users = new List<AppUser>
            {
                new AppUser
                {
                    UserName = "master_overseer",
                    AccessFailedCount = 0,
                    ConcurrencyStamp = "9e3fc90b-9a70-4178-a43b-d3ae05379018",
                    Email = "master_overseer@amraps.local",
                    EmailConfirmed = true,
                    FirstName = "Master",
                    LastName = "Overseer",
                    IsDeleted = false,
                    LockoutEnabled = true,
                    LockoutEnd = null,
                    NormalizedEmail = "MASTER_OVERSEER@AMRAPS.LOCAL",
                    NormalizedUserName = "MASTER_OVERSEER",
                    PasswordHash = "AQAAAAEAACcQAAAAEE9T7Th+W50ABeTaBn726dhcaPS9T+tMx7YQ0EZ54IOayR9sea6d9lN/B4B3l7/6FA==",
                    PhoneNumber = null,
                    PhoneNumberConfirmed = false,
                    SecurityStamp = "983d0920-8d30-45ec-be5f-494f1cefc56c",
                    TwoFactorEnabled = false
                }
            };
            _context.Users.AddRange(users);
            var roles = new List<AppRole>
            {
                new AppRole{ ConcurrencyStamp = Guid.NewGuid().ToString().ToLower() ,
                    Name = "locations", NormalizedName = "LOCATIONS"},
                new AppRole{ ConcurrencyStamp = Guid.NewGuid().ToString().ToLower() ,
                    Name = "locations.readonly", NormalizedName = "LOCATIONS.READONLY"},
                new AppRole{ ConcurrencyStamp = Guid.NewGuid().ToString().ToLower() ,
                    Name = "affiliates", NormalizedName = "AFFILIATES"},
                new AppRole{ ConcurrencyStamp = Guid.NewGuid().ToString().ToLower() ,
                    Name = "affiliates.readonly", NormalizedName = "AFFILIATES.READONLY"},
                new AppRole{ ConcurrencyStamp = Guid.NewGuid().ToString().ToLower() ,
                    Name = "users", NormalizedName = "USERS"},
                new AppRole{ ConcurrencyStamp = Guid.NewGuid().ToString().ToLower() ,
                    Name = "users.readonly", NormalizedName = "USERS.READONLY"}
            };
            _context.Roles.AddRange(roles);

            _context.SaveChanges();
            return this;
        }
    }
}
