USE [DevUp2017]
GO
SET IDENTITY_INSERT [auth].[ApiResources] ON 

GO
INSERT [auth].[ApiResources] ([Id], [Description], [DisplayName], [Enabled], [Name]) VALUES (1, NULL, N'Client Portal Api', 1, N'portal-api')
GO
INSERT [auth].[ApiResources] ([Id], [Description], [DisplayName], [Enabled], [Name]) VALUES (2, NULL, N'Admin Portal Api', 1, N'admin-api')
GO
SET IDENTITY_INSERT [auth].[ApiResources] OFF
GO
SET IDENTITY_INSERT [auth].[ApiClaims] ON 

GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (1, 1, N'locations')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (2, 1, N'locations.readonly')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (3, 1, N'affiliates')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (4, 1, N'affiliates.readonly')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (5, 2, N'locations')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (6, 2, N'locations.readonly')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (7, 2, N'affiliates')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (8, 2, N'affiliates.readonly')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (9, 2, N'users')
GO
INSERT [auth].[ApiClaims] ([Id], [ApiResourceId], [Type]) VALUES (10, 2, N'user.readonly')
GO
SET IDENTITY_INSERT [auth].[ApiClaims] OFF
GO
SET IDENTITY_INSERT [auth].[ApiScopes] ON 

GO
INSERT [auth].[ApiScopes] ([Id], [ApiResourceId], [Description], [DisplayName], [Emphasize], [Name], [Required], [ShowInDiscoveryDocument]) VALUES (1, 1, NULL, N'Client Portal Api', 0, N'portal-api', 0, 1)
GO
INSERT [auth].[ApiScopes] ([Id], [ApiResourceId], [Description], [DisplayName], [Emphasize], [Name], [Required], [ShowInDiscoveryDocument]) VALUES (2, 2, NULL, N'Admin Portal Api', 0, N'admin-api', 0, 1)
GO
SET IDENTITY_INSERT [auth].[ApiScopes] OFF
GO
SET IDENTITY_INSERT [auth].[Clients] ON 

GO
INSERT [auth].[Clients] ([Id], [AbsoluteRefreshTokenLifetime], [AccessTokenLifetime], [AccessTokenType], [AllowAccessTokensViaBrowser], [AllowOfflineAccess], [AllowPlainTextPkce], [AllowRememberConsent], [AlwaysIncludeUserClaimsInIdToken], [AlwaysSendClientClaims], [AuthorizationCodeLifetime], [BackChannelLogoutSessionRequired], [BackChannelLogoutUri], [ClientClaimsPrefix], [ClientId], [ClientName], [ClientUri], [ConsentLifetime], [Description], [EnableLocalLogin], [Enabled], [FrontChannelLogoutSessionRequired], [FrontChannelLogoutUri], [IdentityTokenLifetime], [IncludeJwtId], [LogoUri], [PairWiseSubjectSalt], [ProtocolType], [RefreshTokenExpiration], [RefreshTokenUsage], [RequireClientSecret], [RequireConsent], [RequirePkce], [SlidingRefreshTokenLifetime], [UpdateAccessTokenClaimsOnRefresh]) VALUES (1, 2592000, 3600, 0, 0, 1, 0, 1, 0, 0, 300, 1, NULL, N'client_', N'amraps-portal', N'Amraps Portal', N'https://localhost:4002', NULL, NULL, 1, 1, 1, N'https://localhost:4002/signout-oidc', 300, 0, NULL, NULL, N'oidc', 1, 1, 1, 0, 0, 1296000, 0)
GO
SET IDENTITY_INSERT [auth].[Clients] OFF
GO
SET IDENTITY_INSERT [auth].[ClientCorsOrigins] ON 

GO
INSERT [auth].[ClientCorsOrigins] ([Id], [ClientId], [Origin]) VALUES (1, 1, N'https://localhost:4002')
GO
SET IDENTITY_INSERT [auth].[ClientCorsOrigins] OFF
GO
SET IDENTITY_INSERT [auth].[ClientGrantTypes] ON 

GO
INSERT [auth].[ClientGrantTypes] ([Id], [ClientId], [GrantType]) VALUES (1, 1, N'hybrid')
GO
SET IDENTITY_INSERT [auth].[ClientGrantTypes] OFF
GO
SET IDENTITY_INSERT [auth].[ClientPostLogoutRedirectUris] ON 

GO
INSERT [auth].[ClientPostLogoutRedirectUris] ([Id], [ClientId], [PostLogoutRedirectUri]) VALUES (1, 1, N'https://localhost:4002')
GO
SET IDENTITY_INSERT [auth].[ClientPostLogoutRedirectUris] OFF
GO
SET IDENTITY_INSERT [auth].[ClientRedirectUris] ON 

GO
INSERT [auth].[ClientRedirectUris] ([Id], [ClientId], [RedirectUri]) VALUES (1, 1, N'https://localhost:4002/signin-oidc')
GO
SET IDENTITY_INSERT [auth].[ClientRedirectUris] OFF
GO
SET IDENTITY_INSERT [auth].[ClientScopes] ON 

GO
INSERT [auth].[ClientScopes] ([Id], [ClientId], [Scope]) VALUES (1, 1, N'openid')
GO
INSERT [auth].[ClientScopes] ([Id], [ClientId], [Scope]) VALUES (2, 1, N'profile')
GO
INSERT [auth].[ClientScopes] ([Id], [ClientId], [Scope]) VALUES (3, 1, N'offline_access')
GO
INSERT [auth].[ClientScopes] ([Id], [ClientId], [Scope]) VALUES (4, 1, N'portal-api')
GO
SET IDENTITY_INSERT [auth].[ClientScopes] OFF
GO
SET IDENTITY_INSERT [auth].[ClientSecrets] ON 

GO
INSERT [auth].[ClientSecrets] ([Id], [ClientId], [Description], [Expiration], [Type], [Value]) VALUES (1, 1, NULL, NULL, N'SharedSecret', N'5msdNzAZN+w6XDZqdN3ddOk2aIqUEBWBCvP1/k5q0Vw=')
GO
SET IDENTITY_INSERT [auth].[ClientSecrets] OFF
GO
SET IDENTITY_INSERT [auth].[IdentityResources] ON 

GO
INSERT [auth].[IdentityResources] ([Id], [Description], [DisplayName], [Emphasize], [Enabled], [Name], [Required], [ShowInDiscoveryDocument]) VALUES (1, NULL, N'Your user identifier', 0, 1, N'openid', 1, 1)
GO
INSERT [auth].[IdentityResources] ([Id], [Description], [DisplayName], [Emphasize], [Enabled], [Name], [Required], [ShowInDiscoveryDocument]) VALUES (2, N'Your user profile information (first name, last name, etc.)', N'User profile', 1, 1, N'profile', 0, 1)
GO
INSERT [auth].[IdentityResources] ([Id], [Description], [DisplayName], [Emphasize], [Enabled], [Name], [Required], [ShowInDiscoveryDocument]) VALUES (3, NULL, N'Admin Api', 0, 1, N'admin-api', 0, 1)
GO
INSERT [auth].[IdentityResources] ([Id], [Description], [DisplayName], [Emphasize], [Enabled], [Name], [Required], [ShowInDiscoveryDocument]) VALUES (4, NULL, N'Client Portal Api', 0, 1, N'portal-api', 0, 1)
GO
SET IDENTITY_INSERT [auth].[IdentityResources] OFF
GO
SET IDENTITY_INSERT [auth].[IdentityClaims] ON 

GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (1, 1, N'sub')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (2, 2, N'updated_at')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (3, 2, N'locale')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (4, 2, N'zoneinfo')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (5, 2, N'birthdate')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (6, 2, N'gender')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (7, 2, N'website')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (8, 3, N'admin-api')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (9, 2, N'picture')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (10, 2, N'preferred_username')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (11, 2, N'nickname')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (12, 2, N'middle_name')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (13, 2, N'given_name')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (14, 2, N'family_name')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (15, 2, N'name')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (16, 2, N'profile')
GO
INSERT [auth].[IdentityClaims] ([Id], [IdentityResourceId], [Type]) VALUES (17, 4, N'portal-api')
GO
SET IDENTITY_INSERT [auth].[IdentityClaims] OFF
GO
SET IDENTITY_INSERT [dbo].[Affiliates] ON 

GO
INSERT [dbo].[Affiliates] ([AffiliateId], [CreatedDate], [Name], [EmailAddress], [PhoneNumber], [Website], 
   [PlaceId], [StreetAddress], [StreetAddress2], [City], [State], [PostalCode], [CreatedByUser]) 
VALUES (1, CAST(N'2017-10-11 00:00:00.0000000' AS DateTime2), N'BARx Crossfit LLC', N'info@barxcrossfit.local', N'3145551212', N'https://www.barxcrossfit.com', 
    N'ChIJTx3V1AnO2IcRHgLjBLBl250', N'12309 Old Big Bend Road', NULL, N'Kirkwood', N'MO', N'63122',  N'Master Overseer')
GO
INSERT [dbo].[Affiliates] ([AffiliateId], [CreatedDate], [Name], [EmailAddress], [PhoneNumber], [Website], 
     [PlaceId], [StreetAddress], [StreetAddress2], [City], [State], [PostalCode], [CreatedByUser]) 
VALUES (3, CAST(N'2017-10-11 00:00:00.0000000' AS DateTime2), N'Crossfit 314', N'dl@cf413.local', N'3145551212', N'https://crossfit-314.local', 
     N'ChIJidNTbfjI2IcRpcMrqelxEJE', N'11133 Lindbergh Business Ct', NULL, N'St. Louis', N'MO', N'63123', N'Master Overseer')
GO
INSERT [dbo].[Affiliates] ([AffiliateId], [CreatedDate], [Name], [EmailAddress], [PhoneNumber], [Website], 
     [PlaceId], [StreetAddress], [StreetAddress2], [City], [State], [PostalCode], [CreatedByUser]) 
VALUES (4, CAST(N'2017-10-11 00:00:00.0000000' AS DateTime2), N'Mile High Crossfit', N'mh-info@mhxfit.local', N'4335551212', N'https://mh-xfit.local', 
    N'ChIJFQ1Jd0iPbIcRpVRdPF0AdKU', N'14185 E Easter Ave', NULL, N'Centennial', N'CO', N'80112', N'Master Overseer')
GO
SET IDENTITY_INSERT [dbo].[Affiliates] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

GO
INSERT [dbo].[Users] ([UserId], [AccessFailedCount], [ConcurrencyStamp], [DefaultLocationId], [Email], [EmailConfirmed], [FirstName], [IsDeleted], [LastName], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) VALUES (1, 0, N'9e3fc90b-9a70-4178-a43b-d3ae05379018', NULL, N'master_overseer@amraps.local', 1, N'Master', 1, N'Overseer', 1, NULL, N'MASTER_OVERSEER@AMRAPS.LOCAL', N'MASTER_OVERSEER', N'AQAAAAEAACcQAAAAEE9T7Th+W50ABeTaBn726dhcaPS9T+tMx7YQ0EZ54IOayR9sea6d9lN/B4B3l7/6FA==', NULL, 0, N'983d0920-8d30-45ec-be5f-494f1cefc56c', 0, N'master_overseer')
GO
INSERT [dbo].[Users] ([UserId], [AccessFailedCount], [ConcurrencyStamp], [DefaultLocationId], [Email], [EmailConfirmed], [FirstName], [IsDeleted], [LastName], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) VALUES (2, 0, N'f14f0bc8-1ac9-4ced-8848-c3a21828a954', NULL, N'jeff@amraps.local', 1, N'Jeff', 1, N'St. Germain', 1, NULL, N'JEFF@AMRAPS.LOCAL', N'JEFF_AMRAPS', N'AQAAAAEAACcQAAAAEE9T7Th+W50ABeTaBn726dhcaPS9T+tMx7YQ0EZ54IOayR9sea6d9lN/B4B3l7/6FA==', NULL, 0, N'983d0920-8d30-45ec-be5f-494f1cefc56c', 0, N'jeff_amraps')
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'aHo4QFM7zJOGn7+QZLNpxyzcb7++zmJwK0rFDJEIjHo=', N'amraps-portal', CAST(N'2017-10-11 20:32:22.0000000' AS DateTime2), N'{"CreationTime":"2017-10-11T20:32:22Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-11T20:32:22Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507753938","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-10 20:32:22.0000000' AS DateTime2), N'1', N'refresh_token')
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'F7KBHLXTWtXN5xSpSX3FoQdunnmRERuQOyRehgJoxHA=', N'amraps-portal', CAST(N'2017-10-12 14:48:37.0000000' AS DateTime2), N'{"CreationTime":"2017-10-12T14:48:37Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-12T14:48:37Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507819713","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-11 14:48:37.0000000' AS DateTime2), N'1', N'refresh_token')
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'I9uEGBoW0ff3pEPZ2x5WZEqbRD8Q3PAfgFh7rSLBBoM=', N'amraps-portal', CAST(N'2017-10-12 15:08:06.0000000' AS DateTime2), N'{"CreationTime":"2017-10-12T15:08:06Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-12T15:08:06Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507820883","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-11 15:08:06.0000000' AS DateTime2), N'1', N'refresh_token')
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'iOLfND5Gyr6P6SxpotXWAb5wKj5bYI2ph/4jdriEMLo=', N'amraps-portal', CAST(N'2017-10-11 22:36:33.0000000' AS DateTime2), N'{"CreationTime":"2017-10-11T22:36:33Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-11T22:36:33Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507761365","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-10 22:36:33.0000000' AS DateTime2), N'1', N'refresh_token')
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'jwbQyu1no0chSvGsvdH4r6UV26q1HJK9IVhJa572jOY=', N'amraps-portal', CAST(N'2017-10-12 15:11:47.0000000' AS DateTime2), N'{"CreationTime":"2017-10-12T15:11:47Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-12T15:11:47Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507821104","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-11 15:11:47.0000000' AS DateTime2), N'1', N'refresh_token')
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'Tan8qQvJ7aRpI8itRR3qt2xiPDsqBD1yJeFJP4Mqwic=', N'amraps-portal', CAST(N'2017-10-12 15:09:36.0000000' AS DateTime2), N'{"CreationTime":"2017-10-12T15:09:36Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-12T15:09:36Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507820972","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-11 15:09:36.0000000' AS DateTime2), N'1', N'refresh_token')
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'wMmLpy+GPOuhYUPvheI6N6PdD8qePqsUUULHXUpOWYw=', N'amraps-portal', CAST(N'2017-10-12 01:59:07.0000000' AS DateTime2), N'{"CreationTime":"2017-10-12T01:59:07Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-12T01:59:07Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507773544","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-11 01:59:07.0000000' AS DateTime2), N'1', N'refresh_token')
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'z/L0AFWOdvpHqI2b1ToW04MDFJxig5lMS+wNyPg8sJ8=', N'amraps-portal', CAST(N'2017-10-12 01:35:50.0000000' AS DateTime2), N'{"CreationTime":"2017-10-12T01:35:50Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-12T01:35:50Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507772148","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-11 01:35:50.0000000' AS DateTime2), N'1', N'refresh_token')
GO
INSERT [auth].[PersistedGrants] ([Key], [ClientId], [CreationTime], [Data], [Expiration], [SubjectId], [Type]) VALUES (N'zYDZHQe5fTCT6FXUDx0X67A0KSDWezxtZwMGinZQU6I=', N'amraps-portal', CAST(N'2017-10-12 14:35:09.0000000' AS DateTime2), N'{"CreationTime":"2017-10-12T14:35:09Z","Lifetime":2592000,"AccessToken":{"Audiences":["https://localhost:4000/resources","portal-api"],"Issuer":"https://localhost:4000","CreationTime":"2017-10-12T14:35:09Z","Lifetime":3600,"Type":"access_token","ClientId":"amraps-portal","AccessTokenType":0,"Claims":[{"Type":"client_id","Value":"amraps-portal","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"openid","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"profile","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"portal-api","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"scope","Value":"offline_access","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"sub","Value":"1","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"auth_time","Value":"1507818905","ValueType":"http://www.w3.org/2001/XMLSchema#integer"},{"Type":"idp","Value":"local","ValueType":"http://www.w3.org/2001/XMLSchema#string"},{"Type":"amr","Value":"pwd","ValueType":"http://www.w3.org/2001/XMLSchema#string"}],"Version":4},"Version":4}', CAST(N'2017-11-11 14:35:09.0000000' AS DateTime2), N'1', N'refresh_token')
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

GO
INSERT [dbo].[Roles] ([RoleId], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (3, N'e4fb915c-feaa-4012-8e9f-95788e043166', N'affiliates', N'AFFILIATES')
GO
INSERT [dbo].[Roles] ([RoleId], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (4, N'd299e2cf-21e4-4c3a-8594-96a9f917a195', N'affiliates.readonly', N'AFFILIATES.READONLY')
GO
INSERT [dbo].[Roles] ([RoleId], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (5, N'fb71da55-ce14-4a53-92cc-04c138e5d078', N'locations', N'LOCATIONS')
GO
INSERT [dbo].[Roles] ([RoleId], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (6, N'617d8ce9-c376-40d7-bd47-e646b1dea5b6', N'locations.readonly', N'LOCATIONS.READONLY')
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (3, 1)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (4, 1)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (5, 1)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (5, 2)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (6, 1)
GO
INSERT [dbo].[UserRoles] ([RoleId], [UserId]) VALUES (6, 2)
GO
