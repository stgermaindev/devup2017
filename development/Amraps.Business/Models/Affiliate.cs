﻿using System;
using Amraps.Data.Entities;

namespace Amraps.Business.Models
{
    public class Affiliate
    {
        public Affiliate(){ }

        internal Affiliate(AffiliateEntity entity)
        {
            AffiliateId = entity.AffiliateId;
            Name = entity.Name;
            PhoneNumber = entity.PhoneNumber;
            Website = entity.Website;
            CreatedDate = entity.CreatedDate;
            Address = new Address { 
                PlaceId = entity.PlaceId,
                StreetAddress = entity.StreetAddress,
                StreetAddress2 = entity.StreetAddress2,
                City = entity.City,
                State = entity.State,
                PostalCode = entity.PostalCode
            };
        }

        public int AffiliateId { get; set; }

        public string Name { get; set; }        

        public string PhoneNumber { get; set; }

        public string EmailAddress { get; set; }

        public string Website { get; set; }

        public Address Address { get; set;}

        public DateTime CreatedDate { get; set; }
    }
}
