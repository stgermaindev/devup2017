﻿using System;
using System.Collections.Generic;
using System.Text;
using Amraps.Data.Entities;

namespace Amraps.Business.Models
{
    public class Location
    {
        public Location() { }

        internal Location(LocationEntity entity)
        {
            AffiliateId = entity.AffiliateId;
            AffiliateName = entity.Affiliate?.Name;
            Name = entity.Name;
            CreatedDate = entity.CreatedDate;
        }

        public int LocationId { get; set; }

        public int AffiliateId { get; set; }

        public string AffiliateName { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }
       
    }
}
