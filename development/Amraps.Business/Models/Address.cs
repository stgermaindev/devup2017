using System;
using System.ComponentModel.DataAnnotations;
using Amraps.Data.Entities;

namespace Amraps.Business.Models
{
    public class Address
    {
        public Address(){ }

        public string PlaceId { get; set; }

        public string StreetAddress { get; set; }        

        public string StreetAddress2 { get; set; }      

        public string City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }        
    }
}
