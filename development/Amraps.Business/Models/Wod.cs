using System;
using System.Collections.Generic;

namespace Amraps.Business.Models
{
    public class Wod 
    {
        public Wod()
        {
            Sections = new List<WodSection>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime WodDate { get; set; }
        public string Program { get; set; }
        public List<WodSection> Sections { get; set; }
    }

    public class WodSection 
    {
        public WodSection()
        {
            Components = new List<WodComponent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public List<WodComponent> Components { get; set; }
    }

    public class WodComponent
    {
        public int Id { get; set; }
        public string Name { get; set; }       
    }
}