using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Amraps.Business.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Amraps.Data.Entities;

namespace Amraps.Business.Managers
{
    public interface IWodManager
    {        
        Task<Wod> GetWod(int id);
        Task<Wod> GetTodaysWod();
    }

    public class WodManager : IWodManager
    {
        private readonly AmrapsDbContext _context;
        
        public WodManager(AmrapsDbContext context)
        {
            _context = context;
        }

        public async Task<Wod> GetWod(int id) 
        {
            return await Task.FromResult(new Wod());
        }

        public async Task<Wod> GetTodaysWod() 
        {
            var wod = new Wod
            {
                Id = 1,
                Name = DateTime.Today.ToString("MM/dd/yyyy"),
                WodDate = DateTime.Today                
            };
            var ssSection = new WodSection
            {
                Id = 1,
                Name = "Strength/Skill"
            };
            var metSection = new WodSection
            {
                Id = 2,
                Name = "Metcon (Time)"
            };

            ssSection.Components.Add(new WodComponent
            {
                Id = 1,
                Name = "A1: Ring Rows (4 x Max Reps)"
            });
            ssSection.Components.Add(new WodComponent
            {
                Id = 2,
                Name = "A2: Push-ups (4 x Max Reps)"
            });
            ssSection.Components.Add(new WodComponent
            {
                Id = 3,
                Name = "A3: Hang Power Clean (4 x 5 reps) <br/>**Build weight to warm up for WOD"
            });
            metSection.Components.Add(new WodComponent
            {
                Id = 4,
                Name = @"5 Rounds for time<br/>
                        200m Run<br/>
                        30 AbMat Sit Ups<br/>
                        10 Hang Power Cleans (135/95) or 50% of Clean 1RM"
            });
            wod.Sections.Add(ssSection);
            wod.Sections.Add(metSection);

            return await Task.FromResult(wod);
        }      
    }

    /* 
     A1: Ring Rows (4 x Max Reps)
A2: Push-ups (4 x Max Reps)
A3: Hang Power Clean (4 x 5 reps)
** Build weight to warm up for WOD
B: Metcon (Time)
5 Rounds for time
200m Run
30 AbMat Sit Ups
10 Hang Power Cleans (135/95) or 50% of Clean 1RM
     */
}
