﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Amraps.Business.Models;
using Amraps.Data.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace Amraps.Business.Managers
{
    public interface IAffiliatesManager
    {
        Task<IEnumerable<Affiliate>> AllAffiliates();
        Task<Affiliate> GetAffiliate(int id);
    }

    public class AffiliatesManager : IAffiliatesManager
    {
        private readonly AmrapsDbContext _context;
        
        public AffiliatesManager(AmrapsDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Affiliate>> AllAffiliates()
        {
            return await _context.Affiliates.Select(a => new Affiliate(a)).ToListAsync();
        }

        public async Task<Affiliate> GetAffiliate(int id)
        {
            var entity = await _context.Affiliates.FindAsync(id);
            return entity != null ? new Affiliate(entity) : null;
        }
    }
}
