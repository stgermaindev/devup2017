﻿using System;
using System.Collections.Generic;
using System.Text;
using Amraps.Business.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace Amraps.Business.Extensions
{
    public static class ManagerExtensions
    {
        public static IServiceCollection UseManagers(this IServiceCollection services)
        {
            services.AddTransient<IAffiliatesManager, AffiliatesManager>();
            services.AddTransient<IWodManager, WodManager>();

            return services;
        }
    }
}
