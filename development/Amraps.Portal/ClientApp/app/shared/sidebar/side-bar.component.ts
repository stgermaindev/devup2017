﻿import {Component, ElementRef} from '@angular/core';

export const ROUTES: RouteInfo[] = [
    {
        path: '/wods',
        title: 'Wods',
        type: 'sub',
        icontype: 'fitness_center',
        children: [{
            path: 'todays-wod',
            title: 'Todays Wod',
            ab: 'TW'
        }, {
            path: 'list',
            title: 'List',
            ab: 'LO'
        }]
    }, {
        path: '/athletes',
        title: 'Athletes',
        type: 'sub',
        icontype: 'perm_identity',
        children: [{
            path: 'leads',
            title: 'Leads',
            ab: 'AL'
        },{
            path: 'list',
            title: 'List',
            ab: 'LA'
        }]
    }, {
        path: '/classes',
        title: 'Classes',
        type: 'sub',
        icontype: 'event',
        children: [{
            path: 'list',
            title: 'List',
            ab: 'AA'
        }, {
            path: 'locations',
            title: 'Locations',
            ab: 'LO'
        }]
    }, {
        path: '/affiliates',
        title: 'Affiliates',
        type: 'sub',
        icontype: 'business',
        children: [{
            path: 'list',
            title: 'List',
            ab: 'AA'
        }, {
            path: 'locations',
            title: 'Locations',
            ab: 'LO'
        }]
    }, {
        path: '/financial',
        title: 'Financial',
        type: 'sub',
        icontype: 'attach_money',
        children: [{
            path: 'list',
            title: 'List',
            ab: 'AA'
        }]
    }, {
        path: '/reports',
        title: 'Reports',
        type: 'sub',
        icontype: 'pie_chart',
        children: [{
            path: 'list',
            title: 'List',
            ab: 'AA'
        }]
    }
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './side-bar.component.html'
})
export class SideBarComponent {
    public menuItems: any[];
    public displayName: string;

    constructor(private elemRef: ElementRef) { }

    isNotMobileMenu() {
        const w = window.innerWidth;
        if (w > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.displayName = "Jeff St. Germain";
        const isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
        if (isWindows) {
            // // if we are on windows OS we activate the perfectScrollbar function
            // const sidebar = document.querySelectorAll('.sidebar-wrapper');
            // $sidebar.perfectScrollbar();
            // // if we are on windows OS we activate the perfectScrollbar function
            // jQuery('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
            // jQuery('html').addClass('perfect-scrollbar-on');
        } else {
            const elem = this.elemRef.nativeElement.querySelector('html');
            if (elem)
                elem.addClass('perfect-scrollbar-off');
        }
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
}

export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    // icon: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}