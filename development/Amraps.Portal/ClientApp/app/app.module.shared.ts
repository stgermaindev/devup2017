import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import {AppComponent} from './components/app/app.component';
import {appRoutes, appRoutingProviders} from './app.routes';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AdminModule} from './layouts/admin.module';
import {AffiliatesModule} from './affiliates/affiliates.module';
import {WodsModule} from './wods/wods.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        BrowserAnimationsModule,
        FormsModule,
        AdminModule,
        AffiliatesModule,
        WodsModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [appRoutingProviders]
})
export class AppModuleShared {

}
