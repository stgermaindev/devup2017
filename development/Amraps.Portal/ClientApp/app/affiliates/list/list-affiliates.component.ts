import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Address, Affiliate} from '../models/affiliate.model';

@Component({
    selector: 'list-affiliates',
    templateUrl: './list-affiliates.component.html'
})
export class ListAffiliatesComponent implements OnInit {
    public affiliates: Affiliate[] = [];

    constructor(private route: ActivatedRoute) {

    }

    public ngOnInit(): void {
        this.route.data.subscribe((data) => {
            this.affiliates = data.affiliates;
        });
    }

    buildStreetAddress(address: Address | null): string {
        if(!address)
            return '';

        let addr  = address.streetAddress;
        addr += address.streetAddress2 ? ' ' + address.streetAddress2 : ''

        return addr as string;
    }
}