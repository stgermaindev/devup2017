import {NgModule} from '@angular/core';
import {ListAffiliatesComponent} from './list/list-affiliates.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AffiliatesRouting} from './affiliates.routing';
import {EditAffiliateComponent} from './edit/edit-affiliate.component';
import {AffiliatesService} from './services/affiliates.service';
import {AffiliatesResolver, AffiliateResolver} from './resolvers/affiliates.resolvers';

@NgModule({
    declarations: [
        ListAffiliatesComponent,
        EditAffiliateComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ListAffiliatesComponent,
        EditAffiliateComponent
    ],
    providers: [
        AffiliatesService,
        AffiliatesResolver,
        AffiliateResolver
    ]
})
export class AffiliatesModule {

}



