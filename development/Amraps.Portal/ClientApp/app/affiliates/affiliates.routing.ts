import {Routes} from '@angular/router';
import {ListAffiliatesComponent} from './list/list-affiliates.component';
import {AffiliateResolver, AffiliatesResolver} from './resolvers/affiliates.resolvers';
import {EditAffiliateComponent} from './edit/edit-affiliate.component';
import {AdminLayoutComponent} from '../layouts/admin.layout.component';

export const AffiliatesRouting: Routes = [{
    path: '',
    component: AdminLayoutComponent,
    children: [{
        path: 'affiliates',
        component: ListAffiliatesComponent,
        resolve: {
            affiliates: AffiliatesResolver
        }
    }, {
        path: 'affiliates/edit/:id',
        component: EditAffiliateComponent,
        resolve: {
            affiliate: AffiliateResolver
        }
    }]
}];