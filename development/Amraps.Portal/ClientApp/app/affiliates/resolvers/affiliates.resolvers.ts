import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {AffiliatesService} from '../services/affiliates.service';
import {Affiliate} from '../models/affiliate.model';

@Injectable()
export class AffiliatesResolver implements Resolve<Observable<Affiliate[] | null>> {

    constructor(private affiliatesService: AffiliatesService) { }

    public resolve(route: ActivatedRouteSnapshot):  Observable<Affiliate[] | null>  {
        return this.affiliatesService.getAll();
    }
}

@Injectable()
export class AffiliateResolver implements Resolve<Observable<Affiliate | null>> {

    constructor(private affiliatesService: AffiliatesService) { }

    public resolve(route: ActivatedRouteSnapshot):  Observable<Affiliate | null>  {
        const id = +route.params['id'];
        return this.affiliatesService.get(id);
    }
}