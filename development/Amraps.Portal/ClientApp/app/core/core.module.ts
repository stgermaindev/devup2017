import {NgModule} from '@angular/core';
import {CoreService} from './services/core.service';
import {AuthHttpService} from './auth/auth-http.service';

@NgModule({
    declarations: [],
    imports: [],
    exports: [],
    providers: [
        CoreService,
        AuthHttpService
    ]
})
export class CoreModule {

}