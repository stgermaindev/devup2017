import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Wod} from '../models/wod.model';

@Component({
    selector: 'todays-wod',
    templateUrl: './todays-wod.component.html'
})
export class TodaysWodComponent implements OnInit {
    public wod: Wod;

    constructor(private route: ActivatedRoute) {

    }

    public ngOnInit(): void {
        this.route.data.subscribe((data: any) => {
            this.wod = data.wod;
        });
    }

    public get workout(): string {
        if (!this.wod) {
            return 'Error loading Wod';
        }
        if (!this.wod.sections) {
            return 'Error loading Wod';
        }
        let html = '';
        for(let s of this.wod.sections) {
            if (!s.components) {
                continue;
            }
            html += '<dl>'
            html += `<dt>${s.name}</dt>`
            for(let c of s.components) {
                html += `<dd>${c.name}</dd>`
            }
            html += '</dl>'
        }

        return html;
    }

}