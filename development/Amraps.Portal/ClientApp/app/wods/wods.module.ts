import {NgModule} from '@angular/core';
import {TodaysWodComponent} from './components/todays-wod.component';
import {WodsService} from './services/wods.service';
import {TodaysWodResolver} from './resolvers/wods.resolvers';
import {CommonModule} from '@angular/common';

@NgModule({
    declarations: [TodaysWodComponent],
    imports: [CommonModule],
    exports: [TodaysWodComponent],
    providers: [WodsService, TodaysWodResolver]
})
export class WodsModule {

}