import * as moment from 'moment';

export class Wod implements IWod {
    id: number;
    name?: string | null;
    wodDate: moment.Moment;
    program?: string | null;
    sections?: WodSection[] | null;

    constructor(data?: IWod) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"] !== undefined ? data["id"] : <any>null;
            this.name = data["name"] !== undefined ? data["name"] : <any>null;
            this.wodDate = data["wodDate"] ? moment(data["wodDate"].toString()) : <any>null;
            this.program = data["program"] !== undefined ? data["program"] : <any>null;
            if (data["sections"] && data["sections"].constructor === Array) {
                this.sections = [];
                for (let item of data["sections"])
                    this.sections.push(WodSection.fromJS(item));
            }
        }
    }

    static fromJS(data: any): Wod {
        let result = new Wod();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id !== undefined ? this.id : <any>null;
        data["name"] = this.name !== undefined ? this.name : <any>null;
        data["wodDate"] = this.wodDate ? this.wodDate.toISOString() : <any>null;
        data["program"] = this.program !== undefined ? this.program : <any>null;
        if (this.sections && this.sections.constructor === Array) {
            data["sections"] = [];
            for (let item of this.sections)
                data["sections"].push(item.toJSON());
        }
        return data;
    }
}

export interface IWod {
    id: number;
    name?: string | null;
    wodDate: moment.Moment;
    program?: string | null;
    sections?: WodSection[] | null;
}

export class WodSection implements IWodSection {
    id: number;
    name?: string | null;
    components?: WodComponent[] | null;

    constructor(data?: IWodSection) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"] !== undefined ? data["id"] : <any>null;
            this.name = data["name"] !== undefined ? data["name"] : <any>null;
            if (data["components"] && data["components"].constructor === Array) {
                this.components = [];
                for (let item of data["components"])
                    this.components.push(WodComponent.fromJS(item));
            }
        }
    }

    static fromJS(data: any): WodSection {
        let result = new WodSection();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id !== undefined ? this.id : <any>null;
        data["name"] = this.name !== undefined ? this.name : <any>null;
        if (this.components && this.components.constructor === Array) {
            data["components"] = [];
            for (let item of this.components)
                data["components"].push(item.toJSON());
        }
        return data;
    }
}

export interface IWodSection {
    id: number;
    name?: string | null;
    components?: WodComponent[] | null;
}

export class WodComponent implements IWodComponent {
    id: number;
    name?: string | null;

    constructor(data?: IWodComponent) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"] !== undefined ? data["id"] : <any>null;
            this.name = data["name"] !== undefined ? data["name"] : <any>null;
        }
    }

    static fromJS(data: any): WodComponent {
        let result = new WodComponent();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id !== undefined ? this.id : <any>null;
        data["name"] = this.name !== undefined ? this.name : <any>null;
        return data;
    }
}

export interface IWodComponent {
    id: number;
    name?: string | null;
}