import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Wod} from '../models/wod.model';
import {WodsService} from '../services/wods.service';
import {Injectable} from '@angular/core';

@Injectable()
export class TodaysWodResolver implements Resolve<Observable<Wod | null>> {
    constructor(private wodsService: WodsService) {

    }

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wod | null>  {
        return this.wodsService.getTodaysWod();
    }

}