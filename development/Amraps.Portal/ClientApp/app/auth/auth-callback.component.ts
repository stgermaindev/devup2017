import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {OAuthService} from 'angular-oauth2-oidc';
import {CoreService} from "../core/services/core.service";

@Component({
    selector: 'auth-callback',
    templateUrl: `<div>Completing Authentication Please Wait...</div>`
})
export class AuthCallbackComponent implements OnInit {
    constructor(private router: Router,
        private oauthService: OAuthService,
        private appService: CoreService) {

        this.appService.onAuthorized.subscribe(() => {
            console.log('Auth Callback authorized');
            // this.oauthService.setupAutomaticSilentRefresh();
            this.router.navigateByUrl('/wods/todays-wod');
        });
    }

    ngOnInit(): void {

    }
}