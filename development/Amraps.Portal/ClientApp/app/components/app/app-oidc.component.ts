﻿import {Component, OnInit} from '@angular/core';
import {JwksValidationHandler, LoginOptions, OAuthService} from 'angular-oauth2-oidc';
import {Router} from "@angular/router";
import {CoreService} from "../../core/services/core.service";

@Component({
    selector: 'app-oidc',
    template: `<router-outlet></router-outlet>`
})
export class AppOidcComponent implements OnInit {
    constructor(private oauthService: OAuthService,
                private router: Router,
                private appService: CoreService) {
        
    }

    ngOnInit(): void {
        this.configureAuth();
    }
    
    // Sign in button
    public goToSignIn() {
        this.oauthService.initImplicitFlow();
    }
    
    // Check 
    public get isAuthenticated(): boolean {
        return this.oauthService.hasValidAccessToken();
    }

    public signOut() {
        this.oauthService.logOut(false);
    }

    public gotoHome() {
        this.router.navigate(['wods/todays-wod']);
    }

    private configureAuth() {
        console.log('Initializing authentication');
        this.oauthService.tokenValidationHandler = new JwksValidationHandler();
        this.oauthService.redirectUri = window.location.origin + '/auth/login';
        this.oauthService.postLogoutRedirectUri = window.location.origin;
        this.oauthService.clientId = 'amraps-portal';
        this.oauthService.scope = 'openid profile email portal-api';
        this.oauthService.issuer = this.appService.authorityUrl;
        this.oauthService.clearHashAfterLogin = false;

        this.oauthService.events.subscribe((e:any) => {
            console.log('oauth/oidc event', e);
        });

        this.oauthService.events.filter((e:any) => e.type === 'session_terminated').subscribe((e:any) => {
            console.log('Your session has been terminated!');
        });

        this.oauthService.loadDiscoveryDocument().then(() => {
            // This method just tries to parse the token(s) within the url when
            // the auth-server redirects the user back to the web-app
            // It dosn't send the user the the login page
            this.oauthService.tryLogin({onTokenReceived: (context:any) => {
                this.appService.authorized();
            }}).then(() => {
                if (!this.oauthService.hasValidIdToken()) {
                    this.oauthService.initImplicitFlow(`{"returnTo": ${window.location.href}}`);
                } else {
                    // Silent Refresh token.
                    this.oauthService.setupAutomaticSilentRefresh();
                }
            });
        });
    }
}