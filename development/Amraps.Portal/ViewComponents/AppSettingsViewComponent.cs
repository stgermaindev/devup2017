﻿using System.Threading.Tasks;
using Amraps.Portal.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace Amraps.Portal.Views.Shared
{
    public class AppSettingsViewComponent : ViewComponent
    {
        public AppSettingsViewComponent()
        {
            
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var idToken = await HttpContext.GetTokenAsync("id_token");
            var refreshToken = await HttpContext.GetTokenAsync("refresh_token");

            var settings = new AppSettingsModel
            {
                ApiUrl = "http://localhost:4000/api",
                AuthUrl = "http://localhost:4000",
                WebUrl = "http://localhost:4001",
                AccessToken = token,
                UserName = HttpContext.User.FindFirst("name")?.Value,
                UserDisplayName = HttpContext.User.FindFirst("given_name")?.Value,
                UserId = HttpContext.User.FindFirst("user_id")?.Value
            };
            return View(settings);
        }
    }
}
