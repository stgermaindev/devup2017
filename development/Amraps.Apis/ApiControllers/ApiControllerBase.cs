﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amraps.Apis.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace Amraps.Apis.ApiControllers
{    
    public class ApiControllerBase : Controller
    {
        [SwaggerIgnore]
        public virtual ObjectResult ErrorResult(int code, string message, string referenceNumber = null, IEnumerable<Error> errors = null)
        {
            var response = new ErrorResponse
            {
                ResultCode = code,
                Message = message,
                ReferenceNumber = referenceNumber
            };
            response.Errors.AddRange(errors);
            return StatusCode(code, response);
        }

        [SwaggerIgnore]
        public virtual ObjectResult ErrorResult(int code, ErrorResponse response)
        {
            return StatusCode(code, response);
        }
    }
}
