﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Amraps.Apis.Models;
using Amraps.Business.Managers;
using Amraps.Business.Models;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Microsoft.AspNetCore.Authorization;

namespace Amraps.Apis.ApiControllers.v1
{
    [Authorize(Policy = "PortalApis")]
    [Route("api/v1/affiliates")]
    public class AffiliatesController : ApiControllerBase
    {
        private readonly IAffiliatesManager _affiliatesManager;

        public AffiliatesController(IAffiliatesManager affiliatesManager)
        {
            _affiliatesManager = affiliatesManager;
        }

        [HttpGet]
        [SwaggerResponse(typeof(List<Affiliate>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        public async Task<IActionResult> Get()
        {
            var model = await _affiliatesManager.AllAffiliates();
            return Json(model);
        }

        [HttpGet("{id}")]
        [SwaggerResponse(typeof(Affiliate))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(ErrorResponse))]
        [SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            var model = await _affiliatesManager.GetAffiliate(id);
            if (model != null)
                return Json(model);

            return ErrorResult(400, "Bad Request", null,
                new[] { new Error { Reason = "Bad Request", Message = "Bad Request" } });
        }

    }
}

