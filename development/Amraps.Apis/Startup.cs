﻿using System.Reflection;
using Amraps.Business.Extensions;
using Amraps.Data.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSwag.AspNetCore;

namespace Amraps.Apis
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var amrapsConnection = Configuration.GetConnectionString("AmrapsConnection");
            services.AddDbContext<AmrapsDbContext>(options => options.UseSqlServer(amrapsConnection));

            services.UseManagers();
            services.AddAuthorization(opt =>
            {
                opt.AddPolicy("PortalApis", policy => policy.RequireClaim("scope", "portal-api"));
            });

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            services.AddCors(opt =>
            {
                opt.AddPolicy("default", pol =>
                {
                    pol.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(opt =>
            {
                opt.Authority = "http://localhost:4000";
                opt.RequireHttpsMetadata = false;
                opt.ApiName = "portal-api";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwaggerUi(typeof(Startup).GetTypeInfo().Assembly, new SwaggerUiSettings
            {
                Title = "Amraps Api V1"
            });

            app.UseCors("default");
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
