﻿namespace Amraps.Identity.Models
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
    }
}