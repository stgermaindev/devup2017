﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;

namespace Amraps.Identity.Models
{
    public class IdentityServerErrorModel
    {
        public ErrorMessage IdentityServerError { get; set; }
        public bool IsDevelopment { get; set; }
    }
}
