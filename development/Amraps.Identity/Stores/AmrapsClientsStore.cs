﻿using System.Threading.Tasks;
using Amraps.Identity.Configuration;
using IdentityServer4;
using IdentityServer4.EntityFramework.Interfaces;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.Stores;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Amraps.Identity.Stores
{
    public class AmrapsClientsStore : IClientStore
    {
        private readonly IConfigurationDbContext _context;
        private readonly IdentityServerConfiguration _identityConfig;

        private const string PortalClient = "amraps-portal";
        private readonly IClientStore _baseClientStore;

        public AmrapsClientsStore(IConfigurationDbContext context,
            IOptions<IdentityServerConfiguration> configOptions,
            ILogger<ClientStore> logger)
        {
            _context = context;
            _identityConfig = configOptions.Value;
            _baseClientStore = new ClientStore(context, logger);
        }

        public async Task<Client> FindClientByIdAsync(string clientId)
        {
            var client = await _baseClientStore.FindClientByIdAsync(clientId);
            if (client != null)
                return client;

            //Make sure we have the necessary clients in the db.
            if (clientId == PortalClient)
                return await EnsurePortalClient();

            //******Do other work here to load a client dynamically. Say for access to developer apis.
            return null;
        }


        #region Static Clients
        private async Task<Client> EnsurePortalClient()
        {
            var newClient = new Client
            {
                Enabled = true,
                ClientId = PortalClient,
                ClientUri = _identityConfig.BaseUrls.Web,
                ClientName = "Amraps Portal",
                ClientSecrets =
                {
                    new Secret("17e5e39209994511964c128ea574c673b3779a8a".Sha256())
                },
                AllowedGrantTypes = GrantTypes.Hybrid,
                RequireConsent = false,
                RedirectUris =
                {
                    $"{_identityConfig.BaseUrls.Web}/signin-oidc"
                },
                PostLogoutRedirectUris = { $"{_identityConfig.BaseUrls.Web}" },
                FrontChannelLogoutUri = $"{_identityConfig.BaseUrls.Web}/signout-oidc",
                AllowedCorsOrigins = { _identityConfig.BaseUrls.Web },
                AllowedScopes =
                {
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    IdentityServerConstants.StandardScopes.OfflineAccess,
                    "portal-api"
                },
                AccessTokenType = AccessTokenType.Jwt,
                AccessTokenLifetime = 3600,
                AllowOfflineAccess = true
            };
            _context.Clients.Add(newClient.ToEntity());
            await _context.SaveChangesAsync();
            return newClient;
        }
        #endregion
    }
}
